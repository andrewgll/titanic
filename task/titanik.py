import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    title_prefixes = ['Mr.', 'Mrs.', 'Miss.']
    res = []
    for title in title_prefixes:
        group = df[df['Name'].str.contains(title, regex=False)]
        
        missing_count = group['Age'].isnull().sum()
        median_age = round(group['Age'].median())
        res.append((title,missing_count, median_age))
    return res